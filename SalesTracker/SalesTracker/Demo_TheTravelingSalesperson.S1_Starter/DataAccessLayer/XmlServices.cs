﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Demo_TheTravelingSalesperson
{
    public class XmlServices
    {

        #region Fields
        public string _dataFilePath;
        #endregion

        #region Methods

        public XmlServices(string dataFilePath)
        {
            _dataFilePath = dataFilePath;
        }

        public Salesperson ReadSalespersonFromDataFile()
        {
            Salesperson salesperson = new Salesperson();

            StreamReader sR = new StreamReader(_dataFilePath);

            XmlSerializer deserializer = new XmlSerializer(typeof(Salesperson));

            using (sR)
            {
                object xmlObject = deserializer.Deserialize(sR);
                Console.WriteLine(xmlObject);
                salesperson = (Salesperson)xmlObject;
            }

            return salesperson;
        }

        public void WriteSalespersonToDataFile(Salesperson salesperson)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Salesperson), new XmlRootAttribute("Salesperson"));

            StreamWriter sW = new StreamWriter(_dataFilePath);

            using (sW)
            {
                serializer.Serialize(sW, salesperson);
            }
        }


        #endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo_TheTravelingSalesperson
{
    class Controller
    {
        #region Fields
        private ConsoleView _consoleView;
        private Salesperson _salesPerson;
        private bool _usingApplication;
        #endregion

        #region Methods

        public Controller()
        {
            InitializeController();

            _salesPerson = new Salesperson();
            _consoleView = new ConsoleView();

            ManageApplicationLoop();
        }

        public void Buy()
        {
            int numOfUnits = _consoleView.DisplayGetNumberOfUnitsToBuy(_salesPerson.CurrentStock);
            _salesPerson.CurrentStock.AddProducts(numOfUnits);
        }

        public void Sell()
        {
            int numOfUnits = _consoleView.DisplayGetNumberOfUnitsToSell(_salesPerson.CurrentStock);
            _salesPerson.CurrentStock.SubtractProducts(numOfUnits);

            if (_salesPerson.CurrentStock.OnBackOrder)
            {
                _consoleView.DisplayBackorderNotification(_salesPerson.CurrentStock, numOfUnits);
            }

        }

        public void DisplayInventory()
        {
            _consoleView.DisplayInventory(_salesPerson.CurrentStock);
        }

        private void DisplayLoadAccountInfo()
        {
            bool maxAttemptsExceeded = false;
            bool loadAccountInfo = false;

            if (_salesPerson.AccountID != "")
            {
                loadAccountInfo = _consoleView.DisplayLoadAccountInfo(_salesPerson, out maxAttemptsExceeded);
            } else
            {
                loadAccountInfo = _consoleView.DisplayLoadAccountInfo(out maxAttemptsExceeded);
            }

            if (loadAccountInfo && !maxAttemptsExceeded)
            {
                XmlServices xmlServices = new XmlServices(DataSettings.dataFilePathXml);
                _salesPerson = xmlServices.ReadSalespersonFromDataFile();
                _consoleView.DisplayConfirmLoadAccountInfo(_salesPerson);
            }
        }

        private void DisplaySaveAccountInfo()
        {
            bool maxAttemptsExceeded = false;
            bool saveAccountInfo = false;

            saveAccountInfo = _consoleView.DisplaySaveAccountInfo(_salesPerson, out maxAttemptsExceeded);

            if (saveAccountInfo && !maxAttemptsExceeded)
            {
                XmlServices xmlServices = new XmlServices(DataSettings.dataFilePathXml);

                xmlServices.WriteSalespersonToDataFile(_salesPerson);

                _consoleView.DisplayConfirmSaveAccountInfo();
            }
        }

        /// <summary>
        /// Calls DisplayAccountInfo method (ConsoleView)
        /// </summary>
        public void DisplayAccountInfo()
        {
            _consoleView.DisplayAccountInfo(_salesPerson);
        }

        /// <summary>
        /// Calls DisplayCitiesTraveled method (ConsoleView)
        /// </summary>
        public void DisplayCities()
        {
            _consoleView.DisplayCitiesTraveled(_salesPerson);

        }

        /// <summary>
        /// Instantiates Salesperson and ConsoleView objets
        /// </summary>
        public void InitializeController()
        {
            _usingApplication = true;
        }

        /// <summary>
        /// method to manage the application setup and control loop
        /// </summary>
        private void ManageApplicationLoop()
        {
            MenuOption userMenuChoice;

            _consoleView.DisplayWelcomeScreen();

            //
            // application loop
            //
            while (_usingApplication)
            {

                //
                // get a menu choice from the ConsoleView object
                //
                userMenuChoice = _consoleView.DisplayGetUserMenuChoice();

                //
                // choose an action based on the user's menu choice
                //
                switch (userMenuChoice)
                {
                    case MenuOption.None:
                        break;
                    case MenuOption.SetupAccount:
                        SetupAccount();
                        break;
                    case MenuOption.Travel:
                        Travel();
                        break;
                    case MenuOption.Buy:
                        Buy();
                        break;
                    case MenuOption.Sell:
                        Sell();
                        break;
                    case MenuOption.DisplayInventory:
                        DisplayInventory();
                        break;
                    case MenuOption.DisplayCities:
                        DisplayCities();
                        break;
                    case MenuOption.DisplayAccountInfo:
                        DisplayAccountInfo();
                        break;
                    case MenuOption.SaveAccountInfo:
                        DisplaySaveAccountInfo();
                        break;
                    case MenuOption.LoadAccountInfo:
                        DisplayLoadAccountInfo();
                        break;
                    case MenuOption.Exit:
                        _usingApplication = false;
                        break;
                    default:
                        break;
                }
            }

            _consoleView.DisplayClosingScreen();

            //
            // close the application
            //
            Environment.Exit(1);
        }

        private void SetupAccount()
        {
            _salesPerson = _consoleView.DisplaySetupAccount();
        }

        /// <summary>
        /// Calls the DisplayGetNextCity method (ConsoleView)
        /// </summary>
        public void Travel()
        {
            string _nextCity;

            _nextCity = _consoleView.DisplayGetNextCity();

            _salesPerson.CitiesVisited.Add(_nextCity);
        }

        #endregion

    }
}

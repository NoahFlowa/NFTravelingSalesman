﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo_TheTravelingSalesperson.Models
{

    /// <summary>
    /// All menu option
    /// </summary>
    public enum MenuOption
    {
        None,
        Travel,
        Buy,
        Sell,
        DisplayInventory,
        DispayCities,
        DisplayAccountInfo,
        Exit
    }
}

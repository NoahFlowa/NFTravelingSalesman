﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo_TheTravelingSalesperson
{
    public class Product
    {
        #region Enums
        public enum ProductType
        {
            None,
            Furry,
            Spotted,
            Dancing
        }
        #endregion

        #region Fields

        private int _numOfUnits;
        private bool _onBackOrder;
        private ProductType _type;

        #endregion

        #region Properties

        public ProductType Type
        {
            get { return _type; }
            set { _type = value; }
        }

        public bool OnBackOrder
        {
            get { return _onBackOrder; }
            set { _onBackOrder = value; }
        }

        public int NumberOfUnits
        {
            get { return _numOfUnits; }
        }

        #endregion

        #region Methods

        public void AddProducts(int unitsToAdd)
        {
            _numOfUnits += unitsToAdd;
        }

        public void SubtractProducts(int unitsToSubtract)
        {
            if (_numOfUnits < unitsToSubtract)
            {
                _onBackOrder = true;
            }
            _numOfUnits -= unitsToSubtract;
        }

        #endregion

        #region Constructors

        public Product()
        {
            _type = ProductType.None;
            _numOfUnits = 0;
        }

        public Product(ProductType type, int numOfUnits)
        {
            _type = type;
            _numOfUnits = numOfUnits;
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo_TheTravelingSalesperson
{
    #region Enums

    public enum MenuOption{
        None,
        SetupAccount,
        Travel,
        Buy,
        Sell,
        DisplayInventory,
        DisplayCities,
        DisplayAccountInfo,
        SaveAccountInfo,
        LoadAccountInfo,
        Exit
    }

    #endregion


    public class Salesperson
    {
        #region Fields

        private string _accountID;
        private List<string> _citiesVisited;
        private string _firstName;
        private string _lastName;
        private Product _currentStock;

        #endregion

        #region Properties
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }
        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }
        public List<string> CitiesVisited
        {
            get { return _citiesVisited; }
            set { _citiesVisited = value; }
        }
        public string AccountID
        {
            get { return _accountID; }
            set { _accountID = value; }
        }
        public Product CurrentStock
        {
            get { return _currentStock; }
            set { _currentStock = value; }
        }
        #endregion

        #region Methods

        public Salesperson()
        {
            _citiesVisited = new List<string>();
            _currentStock = new Product();
        }

        public Salesperson(string firstName, string lastName, string accountID)
        {
            _firstName = firstName;
            _lastName = lastName;
            _accountID = accountID;

            _citiesVisited = new List<string>();
            _currentStock = new Product();
        }

        #endregion

    }
}

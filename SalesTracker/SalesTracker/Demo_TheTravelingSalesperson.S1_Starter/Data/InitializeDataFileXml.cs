﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo_TheTravelingSalesperson
{
    public class InitializeDataFileXml
    {

        private Salesperson InitializeSalesperson()
        {

            Salesperson salesperson = new Salesperson()
            {
                FirstName = "Jhon",
                LastName = "Smith",
                AccountID = "banana123",
                CurrentStock = new Product(Product.ProductType.Furry, 20),
                CitiesVisited = new List<string>()
                {
                    "Detroit",
                    "Traverse City",
                    "Grand Rapids"
                }
            };

            return salesperson;

        }

        public void SeedDataFile()
        {
            XmlServices xmlService = new XmlServices(DataSettings.dataFilePathXml);
            xmlService.WriteSalespersonToDataFile(InitializeSalesperson());
        }

    }
}
